/* 
 * File:   main.cpp
 * Author: danmax
 *
 * Created on 3 декабря 2018 г., 23:07
 */

#include "rule.h"


using namespace std;


int main(int argc, char** argv) {
    string argvs[] = {"-pin", "-pout", "-k", "-h", "-s", "-d", "-e"};
    bool usedArgvs[7]  = {false};
    unsigned char key[keysize];
    string filein, fileout;
    for(int i = 0; i < argc; ++i){
        for(int j = 0; j < 7; ++j){
            if(argv[i] == argvs[j]){
                usedArgvs[j] = true;
                if(j == 0){
                    filein = argv[i + 1];
                }
                if(j == 1){
                    fileout = argv[i + 1];
                }
                if(j == 2){
                    for(int k = 0; k < keysize; ++k){
                        key[k] = (unsigned char)atoi(argv[i + k + 1]);
                    }
                }
                if(j == 3){
                    cout<<"Help:\n\t-p_in input file"<<endl;
                    cout<<"\t-p_out output file"<<endl;
                    cout<<"\t-k input key: 8 ints in the range 0 - 255"<<endl;
                    cout<<"\t-s show generated keys"<<endl;
                    cout<<"\t-d decrypt"<<endl;
                    cout<<"\t-e encrypt"<<endl;
                    cout<<"\t-h help"<<endl;
                    return 2;
                }
            }
        }
    }
    if(usedArgvs[2]){
        Rule gen(key, keysize);
        if(usedArgvs[5]){
            vector<uc> in = gen.read(filein);
            vector<uc> out = gen.encrypt(in);
            gen.write(fileout,out);
        }
        if(usedArgvs[6]){
            vector<uc> in = gen.read(filein);
            vector<uc> out = gen.encrypt(in);
            gen.write(fileout,out);
        }
    }
    else{
//        Rule gen();
//        if(usedArgvs[5]){
//            gen.write(fileout,gen.encrypt(gen.read(filein)));
//        }
//        if(usedArgvs[6]){
//            gen.write(fileout,gen.decrypt(gen.read(filein)));
//        }
    }
    
    return 0;
}

