/* 
 * File:   cipher.cpp
 * Author: danmax
 *
 * Created on 3 декабря 2018 г., 23:09
 */
#include "rule.h"

Rule::Rule(){}

Rule::~Rule(){}

Rule::Rule(uc* keys, int size){
    if(size == keysize){
        key = new uc[keysize];
        for(int i = 0; i < keysize; ++i){
            key[i] = keys[i];
        }
    }
}

int Rule::mod(int x, int m){
    return (x % m + m) % m;
}

vector<uc> Rule::read(string name){
    FILE *input;
    if ((input=fopen(name.c_str(), "rb"))==NULL) {
        printf ("Cannot open file.\n");
    }
    vector<uc> res;
    uc t;
    while(!feof(input)){
        fread(&t,sizeof(t),1,input);
        res.push_back(t);
    }
    vector<uc> temp;
    for(int i = 0; i < res.size() - 1; ++i){
        temp.push_back(res[i]);
    }
    for(int i = 0; i < temp.size(); ++i){
        cout<<temp[i]<< " ";
    }
    cout<<endl;
    fclose(input);
    return temp;
}

void Rule::write(string name, vector<uc> in){
    FILE *output;
    if ((output=fopen(name.c_str(), "wb"))==NULL) {
        printf ("Cannot open file.\n");
        return;
    }
    uc t;
    for(int i = 0; i < in.size(); ++i){
        t = in[i];
        fwrite(&t,sizeof(t),1,output);
    }
    fclose(output);
}

vector<uc> Rule::encrypt(const vector<uc> in){
    vector<uc> key = generateGamma(in.size());
    vector<uc> res;
    for(int i = 0; i < in.size(); ++i){
        res.push_back(in[i] ^ key[i]);
    }
    return res;
}

vector<uc> Rule::decrypt(const vector<uc> in){
    vector<uc> key = generateGamma(in.size());
    vector<uc> res;
    for(int i = 0; i < in.size(); ++i){
        res.push_back(in[i] ^ key[i]);
    }
    return res;
}

vector<uc> Rule::generateGamma(int N){
    vector<uc> res; 
    vector<uc> temp;
    for(int i = 0; i < N; ++i){
        res.push_back( key[i % keysize]);
    }
    for(int k = 0; k < N - 1; k++){
        for(int i = 0; i < N; ++i){
           temp.push_back(res[mod(i - 1, N)] ^ (res[mod(i, N)] | res[mod(i + 1, N)])) ;
        }
        res.clear();
        res = temp;
        temp.clear();
    }
    return res;
}