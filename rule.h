/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   cipher.h
 * Author: danmax
 *
 * Created on 3 декабря 2018 г., 23:11
 */
#include <vector>
#include <ctime>
#include <string>
#include <iostream>

#ifndef CIPHER_H
#define CIPHER_H
#define uc unsigned char
#define keysize 4
#define updatecount 1
using namespace std;

class Rule
{
private:
    uc* key;
    vector<uc> generateGamma(int N);
    int mod(int x, int m);
public:
    vector<uc> read(string name);
    void write(string name, vector<uc> in);
    vector<uc> decrypt(const vector<uc> in);
    vector<uc> encrypt(const vector<uc> in);
    void runTests();
    Rule(uc* keys, int size);
    Rule();
    ~Rule();
};



#endif /* CIPHER_H */

