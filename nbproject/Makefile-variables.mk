#
# Generated - do not edit!
#
# NOCDDL
#
CND_BASEDIR=`pwd`
CND_BUILDDIR=build
CND_DISTDIR=dist
# Debug configuration
CND_PLATFORM_Debug=Cygwin-Windows
CND_ARTIFACT_DIR_Debug=dist/Debug/Cygwin-Windows
CND_ARTIFACT_NAME_Debug=cipher3
CND_ARTIFACT_PATH_Debug=dist/Debug/Cygwin-Windows/cipher3
CND_PACKAGE_DIR_Debug=dist/Debug/Cygwin-Windows/package
CND_PACKAGE_NAME_Debug=cipher3.tar
CND_PACKAGE_PATH_Debug=dist/Debug/Cygwin-Windows/package/cipher3.tar
# Release configuration
CND_PLATFORM_Release=Cygwin-Windows
CND_ARTIFACT_DIR_Release=dist/Release/Cygwin-Windows
CND_ARTIFACT_NAME_Release=cipher3
CND_ARTIFACT_PATH_Release=dist/Release/Cygwin-Windows/cipher3
CND_PACKAGE_DIR_Release=dist/Release/Cygwin-Windows/package
CND_PACKAGE_NAME_Release=cipher3.tar
CND_PACKAGE_PATH_Release=dist/Release/Cygwin-Windows/package/cipher3.tar
# encrypt configuration
CND_PLATFORM_encrypt=Cygwin-Windows
CND_ARTIFACT_DIR_encrypt=dist/encrypt/Cygwin-Windows
CND_ARTIFACT_NAME_encrypt=cipher3
CND_ARTIFACT_PATH_encrypt=dist/encrypt/Cygwin-Windows/cipher3
CND_PACKAGE_DIR_encrypt=dist/encrypt/Cygwin-Windows/package
CND_PACKAGE_NAME_encrypt=cipher3.tar
CND_PACKAGE_PATH_encrypt=dist/encrypt/Cygwin-Windows/package/cipher3.tar
# encrypt_bmp configuration
CND_PLATFORM_encrypt_bmp=Cygwin-Windows
CND_ARTIFACT_DIR_encrypt_bmp=dist/encrypt_bmp/Cygwin-Windows
CND_ARTIFACT_NAME_encrypt_bmp=cipher3
CND_ARTIFACT_PATH_encrypt_bmp=dist/encrypt_bmp/Cygwin-Windows/cipher3
CND_PACKAGE_DIR_encrypt_bmp=dist/encrypt_bmp/Cygwin-Windows/package
CND_PACKAGE_NAME_encrypt_bmp=cipher3.tar
CND_PACKAGE_PATH_encrypt_bmp=dist/encrypt_bmp/Cygwin-Windows/package/cipher3.tar
#
# include compiler specific variables
#
# dmake command
ROOT:sh = test -f nbproject/private/Makefile-variables.mk || \
	(mkdir -p nbproject/private && touch nbproject/private/Makefile-variables.mk)
#
# gmake command
.PHONY: $(shell test -f nbproject/private/Makefile-variables.mk || (mkdir -p nbproject/private && touch nbproject/private/Makefile-variables.mk))
#
include nbproject/private/Makefile-variables.mk
